//go:generate goagen -d gopherjs/design app
//go:generate goagen -d gopherjs/design main
//go:generate goagen -d gopherjs/design client
//go:generate goagen -d gopherjs/design swagger
//go:generate gopherjs build gopherjs/public -o public/website.js
//go:generate mkdir -p images
package main
