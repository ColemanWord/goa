//go:generate goagen -d gitlab.com/colemanword/goa/files/design app
//go:generate goagen -d gitlab.com/colemanword/goa/files/design main
//go:generate goagen -d gitlab.com/colemanword/goa/files/design client
//go:generate goagen -d gitlab.com/colemanword/goa/files/design swagger -o public
//go:generate goagen -d gitlab.com/colemanword/goa/files/design schema -o public
//go:generate go-bindata -ignore bindata.go -pkg swagger -o public/swagger/bindata.go ./public/swagger/...
package main
